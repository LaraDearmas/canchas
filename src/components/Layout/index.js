import React from 'react';
import clsx from 'clsx';
import {withStyles } from '@material-ui/core';
import styles from './styles';

class Layout extends React.Component {

    render() {
        const { classes, children, fondo } = this.props;
        return <div className={clsx({
            [classes.home]: fondo === 'menu',
            [classes.general]: true,
        })}>
            <div className={classes.main}>
                {children}
            </div>
        </div>
    }
}

export default withStyles(styles)(Layout);