export default theme => ({
    home: {
        background: 'url(background2.jpg) no-repeat center center fixed',
        backgroundSize: 'cover',
        height: '100vh',
        /*'&:before': {
            content: '""',
            backgroundColor: 'rgba(0,0,0,0.2)',
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
        },*/
    },
    main: {
        position: 'relative',
        margin: '0 auto',
        width: '80%',
        height: 'calc(100vh - 112px)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
