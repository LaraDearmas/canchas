export default theme => ({
    grid: {
        textAlign: "right",
    },
    gridredes: {
        textAlign: "center",
    },
    titleppl: {
        fontFamily: 'Montserrat , sans-serif',
        fontSize: 35,
        color: '#fff',
        letterSpacing: 2,
        fontWeight: 800,
    },
    txtppl: {
        fontFamily: 'Montserrat , sans-serif',
        fontSize: 17,
        color: '#fff',
        letterSpacing: 2,
        fontWeight: 100,
    },
    button: {
        fontFamily: 'Montserrat',
        fontSize: '25px',
        fontWeight: '600',
        letterSpacing: '3px',
        margin: theme.spacing(1),
        color: '#fff',
        borderColor: '#fff',
        "&:hover": {
            backgroundColor: '#1a237e',
            borderColor: '#1a237e',
        },
    },
    redes: {
        margin: 20,
        color: 'rgba(255,255,255,0.75)',
    },
});
