import React from 'react';
import Layout from '../../components/Layout';
import { Grid, Button, withStyles } from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import styles from './styles';

function Home({classes}){
    return <Layout fondo="menu">
                <Grid container>
                    <Grid className={classes.grid} item xs={12} sm={6}>

                    </Grid>
                    <Grid className={classes.grid} item xs={12} sm={6}>
                        <h1 className={classes.titleppl}>AHORA RESERVAR TU CANCHA ES MUCHO MAS FÁCIL</h1>
                        <p className={classes.txtppl}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                        laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <Button variant="outlined" className={classes.button}>
                            RESERVAR CANCHA
                        </Button>
                    </Grid>
                    <Grid className={classes.gridredes} item xs={12}>
                        <WhatsAppIcon className={classes.redes} fontSize="large"/>
                        <InstagramIcon className={classes.redes} fontSize="large"/>
                        <FacebookIcon className={classes.redes} fontSize="large"/>
                    </Grid>
                </Grid>
            </Layout>
}
export default withStyles(styles)(Home);